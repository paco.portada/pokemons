# Ejemplo de Pokemons actualizado

Se muestran en pantalla los nombres e imágenes de Pokemons.

Una app que usa Retrofit para consumir [PokeAPI](https://pokeapi.co/) y carga imágenes usando Glide.

<img src="pokemons.png" alt="pokemons" />

Tutorial original:
https://alvarez.tech/tutorials/android-consumir-pokemon-api-retrofit/

Código original en GitHub:
https://github.com/alvareztech/Pokedex

